package com.example.taskh;

import com.example.taskh.model.RequestModel;

import java.util.ArrayList;
import java.util.List;

class Convertor {

    public static List<Country> convert(List<RequestModel> requestModels) {
        List<Country> countries = new ArrayList<>();
        for (int i = 0; i < requestModels.size(); i++) {
            if (requestModels.get(i).getArea() == null)
                countries.add(new Country(i + 1, requestModels.get(i).getName(), requestModels.get(i).getPopulation(), 0d));
            else
                countries.add(new Country(i + 1, requestModels.get(i).getName(), requestModels.get(i).getPopulation(), requestModels.get(i).getArea()));

        }
        return countries;
    }
}

