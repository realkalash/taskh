package com.example.taskh;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;

import com.example.taskh.adapters.AdapterListCountries;
import com.example.taskh.dataBases.CountriesDataBase;
import com.example.taskh.dataBases.CountriesDataBase_Impl;
import com.example.taskh.model.RequestModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


public class MainActivity extends AppCompatActivity {
    @BindView(R.id.amRecyclerCountry)
    RecyclerView recyclerView;
    @BindView(R.id.amEditText)
    EditText txtInputPopulation;
    @BindView(R.id.amButton)
    Button button;
    CountriesDataBase countriesDataBase;

    List<Country> countryList = new ArrayList<>();

    AdapterListCountries  adapterListCountries = new AdapterListCountries(countryList,this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        countriesDataBase = Room.databaseBuilder(this, CountriesDataBase.class, "database")
                .fallbackToDestructiveMigration() // уничтожаются все таблицы и создаются заоново при миграции версий БД
                .build();

        recyclerView.setAdapter(adapterListCountries);


    }

    @SuppressLint("CheckResult")
    @OnClick(R.id.amButton)
    public void getData() {

        /*Запрос на сервер и установка из сервера в БД*/
        Disposable disposable = ApiService.getAllCountries()
                .map(new Function<List<RequestModel>, List<Country>>() {
                    @Override
                    public List<Country> apply(List<RequestModel> requestModels) {
                        List<Country> countries = Convertor.convert(requestModels);
                        countriesDataBase.getCountryDAO().removeAll();
                        countriesDataBase.getCountryDAO().insertAll(countries);

                        return countries;
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Consumer<List<Country>>() {
                    @Override
                    public void accept(List<Country> list) throws Exception {
                        /* DO NOTHING */
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) throws Exception {
//                        Log.d("name", throwable.getMessage());
                        throwable.printStackTrace();
                        Toast.makeText(MainActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
                    }
                });

        /*Установка из БД в адаптер*/
       Flowable<List<Country>> requestModelFlowable = countriesDataBase.getCountryDAO().selectAll();
       requestModelFlowable.observeOn(AndroidSchedulers.mainThread())
               .subscribeOn(Schedulers.io())
               .subscribe(new Consumer<List<Country>>() {
                   @Override
                   public void accept(List<Country> countries) throws Exception {
                       adapterListCountries.updateList(countries);
                   }
               });


    }



}



