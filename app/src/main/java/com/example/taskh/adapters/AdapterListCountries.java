package com.example.taskh.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.taskh.Country;
import com.example.taskh.R;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AdapterListCountries extends RecyclerView.Adapter<AdapterListCountries.ViewHolder> {

    List<Country> requestModels;
    Context context;

    public AdapterListCountries(List<Country> requestModels, Context context) {
        this.requestModels = requestModels;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recycler_view_item, parent);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder view, int position) {
        view.bind(position);

    }

    public void updateList(List<Country> newList){
        requestModels.clear();
        requestModels = newList;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return requestModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.rviImageCountry)
        ImageView imageCountry;
        @BindView(R.id.rviNameCountry)
        TextView txtNameCountry;
        @BindView(R.id.rviPopulationCountry)
        TextView txtPopulationCountry;
        @BindView(R.id.rviAnother)
        TextView txtAnother;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(int position) {
//            imageCountry.setImageResource(R.drawable.);
            txtNameCountry.setText(requestModels.get(position).getName());
            txtPopulationCountry.setText(requestModels.get(position).getPopulation());
            txtAnother.setText(String.format(Locale.ENGLISH,"%f.2", requestModels.get(position).getArea()));

        }
    }
}
