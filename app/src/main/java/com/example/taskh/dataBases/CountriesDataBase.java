package com.example.taskh.dataBases;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.taskh.Country;
import com.example.taskh.model.RequestModel;

@Database(entities = {Country.class}, version = 4)
public abstract class CountriesDataBase extends RoomDatabase{
    public abstract CountryDAO getCountryDAO();
}
