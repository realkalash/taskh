package com.example.taskh.dataBases;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.taskh.Country;
import com.example.taskh.model.RequestModel;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public abstract class CountryDAO  {

    @Insert
    public abstract void insertAll(List<Country> countries);

    @Query("SELECT * FROM Country")                                // позволяет использовать буферизацию. Когда выдираем большое кол-во данных, можно выдирать только по чуть-чуть к примеру обьектов
    public abstract Flowable<List<Country>> selectAll();                     // тот же обзёрв


    @Query("DELETE FROM Country")
    public abstract void removeAll();

}
